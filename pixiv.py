from pixivpy3 import AppPixivAPI
import re
from gppt import GetPixivToken


class Pixiv:
    api = None

    def __init__(self, url):
        self.raw_info = None
        self.original_url = url
        self.id = re.findall(r"www\.pixiv\.net/artworks/(\d+)", url)[0]

    @staticmethod
    def is_valid_url(url):
        return re.match(r"^.*\.pixiv\.net/artworks/.*$", url)

    @staticmethod
    def init_client(username, password):
        g = GetPixivToken()
        res = g.login(headless=True, username=username, password=password)
        Pixiv.api = AppPixivAPI()
        Pixiv.api.set_auth(access_token=res["access_token"], refresh_token=res["refresh_token"])

    def info_request(self):
        self.raw_info = self.api.illust_detail(self.id)

    def image_urls(self):
        if self.raw_info["illust"]["meta_single_page"]:
            return [self.raw_info["illust"]["meta_single_page"]["original_image_url"]]
        return [image["image_urls"]["original"] for image in self.raw_info["illust"]["meta_pages"]]

    def is_nsfw(self):
        tags = [tag["name"] for tag in self.raw_info['illust']['tags']]
        return "R-18" in tags

    def username(self):
        return self.raw_info['illust']['user']["name"]

    def userid(self):
        return self.raw_info['illust']['user']["name"]

    def url(self):
        return self.original_url
