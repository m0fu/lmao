import requests
import re
import lxml.html
from lxml.etree import tostring


class Twitter:
    UA = {'User-Agent': 'facebookexternalhit/1.1'}

    def __init__(self, url):
        self._from_fx = None
        self.raw_info = None
        self.original_url = url
        self._nsfw = False
        [[self._userid, self.id]] = re.findall(r"twitter\.com/([a-zA-Z0-9_]{1,15})/status/(\d+)", url)

    @staticmethod
    def is_valid_url(url):
        return re.match(r"^.*twitter\.com/.*$", url)

    def info_request(self):
        html_text = requests.get(self.original_url, headers=self.UA).text
        if "To view this media, you’ll need to" in html_text:
            raise NSFWTweetException(FxTwitter(self._userid, self.id))
        self.raw_info = lxml.html.fromstring(html_text)

    def image_urls(self):
        article = tostring(self.raw_info.xpath("/html/body/div/div/div/div/main/div/div/div/div/div/div/section/div"
                                               "/div/div[1]/div/article")[0])
        results = list(set(re.findall(r"(https://pbs.twimg.com/media/[a-zA-Z0-9\-_]+)", str(article))))
        for i, url in enumerate(results):
            results[i] = f'{url}.jpg:orig'
        return results

    def is_nsfw(self):
        return self._nsfw or None

    def username(self):
        return self.raw_info.xpath(
            "/html/body/div/div/div/div/main/div/div/div/div/div/div/section/div/div/div[1]/div/article/div/div/div[2]/div[2]/div/div/div/div/div/div[1]/div/a/div/div[1]/span/span")[0].text

    def userid(self):
        return self._userid

    def url(self):
        return f"https://twitter.com/{self._userid}/status/{self.id}"


class NSFWTweetException(Exception):
    def __init__(self, fx_twitter_object):
        self.fx_twitter_object = fx_twitter_object

    def next_obj(self):
        return self.fx_twitter_object


class FxTwitter:
    UA = {'User-Agent': 'facebookexternalhit/1.1'}

    def __init__(self, user_id, status_id, nsfw=True):
        self.user_id = user_id
        self.status_id = status_id
        self.raw_info = None
        self._nsfw = nsfw
        self.original_url = f"https://fxtwitter.com/{self.user_id}/status/{self.status_id}"

    def info_request(self):
        html_text = requests.get(self.original_url, headers=self.UA).text
        self.raw_info = lxml.html.fromstring(html_text)

    def image_urls(self):
        fx_img_url = self.raw_info.xpath('//meta[@property="twitter:image"]')[0].get("content")
        img_ids = re.findall(r"twitter\.com/jpeg/\d+/([\w/]+)", fx_img_url)[0].split("/")
        return [f"https://pbs.twimg.com/media/{id}.jpg:orig" for id in img_ids]

    def is_nsfw(self):
        return self._nsfw or None

    def whole_username(self):
        username_whole = self.raw_info.xpath('//meta[@property="twitter:title"]')[0].get("content")
        return re.findall(r"(.*?) \(@(.*?)\)", username_whole)[0]

    def username(self):
        return self.whole_username()[0]

    def userid(self):
        return self.whole_username()[1]

    def url(self):
        return f"https://twitter.com/{self.user_id}/status/{self.status_id}"
