from os import environ
import telebot
from pixiv import Pixiv
from processing import processing
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-p", "--proxy", action='store_true', help="use proxy")
store = parser.parse_args()

bot = telebot.TeleBot(environ["BOT_TOKEN"], parse_mode="html", )


@bot.message_handler(func=lambda message: True)
def formatter(message):
    link_list = list(set(message.text.split()))
    print(f"incoming {len(link_list)} links")
    for input_url in link_list:
        obj = processing(input_url)
        if not obj:
            print(f"url {input_url} is not processable")
            continue
        print(f"link: {input_url} has {len(obj.image_urls())} image{'s' if len(obj.image_urls()) != 1 else ''}")

        for link in obj.image_urls():
            caption = f'<a href="{obj.url()}">{str(obj.username())}</a>\n{f"#{str(obj.userid())}" if obj.userid() else ""}{" #nsfw" if obj.is_nsfw() else ""}'
            print(f"caption: {caption}")
            print(f"link: {link}")
            bot.send_photo(
                chat_id=message.chat.id,
                photo=link,
                caption=caption
            )


if __name__ == '__main__':
    Pixiv.init_client(environ["PIXIV_USERNAME"], environ["PIXIV_PASSWORD"])
    bot.infinity_polling()
